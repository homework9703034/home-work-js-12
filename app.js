const btn = document.querySelectorAll(".btn");
document.addEventListener("keydown", (event) => {
  const wrapper = document.querySelectorAll("button");
  for (let i = 0; i < wrapper.length; i++) {
    wrapper[i].classList.remove("active");
  }

  const data = document.getElementById(event.key);
  if (!data) {
    return;
  }
  data.classList.add("active");
});
